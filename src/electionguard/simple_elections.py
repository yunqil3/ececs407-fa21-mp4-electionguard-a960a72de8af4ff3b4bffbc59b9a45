from typing import Optional, Tuple, List, Dict, Final

from electionguard.chaum_pedersen import (
    make_disjunctive_chaum_pedersen_known_nonce,
    make_constant_chaum_pedersen_proof_known_nonce,
    make_constant_chaum_pedersen_proof_known_secret_key,
)
from electionguard.elgamal import elgamal_encrypt, elgamal_add, ElGamalCiphertext
from electionguard.group import ElementModQ, add_q, rand_range_q
from electionguard.logs import log_error
from electionguard.nonces import Nonces
from electionguard.simple_election_data import (
    CiphertextBallot,
    PlaintextBallot,
    PlaintextSelection,
    CiphertextSelection,
    PrivateElectionContext,
    CiphertextSelectionTally,
    AnyElectionContext,
    PlaintextSelectionWithProof,
    PlaintextBallotWithProofs,
)
from electionguard.utils import list_of_option_to_option_list

PLACEHOLDER_NAME: Final[str] = "PLACEHOLDER"


def encrypt_selection(
    context: AnyElectionContext,
    selection: PlaintextSelection,
    seed_nonce: ElementModQ,
) -> Tuple[CiphertextSelection, ElementModQ]:
    """
    Given a selection and the necessary election context, encrypts the selection and returns the
    encrypted selection plus the encryption nonce. If anything goes wrong, `None` is returned.
    """

    # TODO: implement this for part 1
    raise RuntimeError("not implemented yet")


def encrypt_ballot(
    context: AnyElectionContext,
    ballot: PlaintextBallot,
    seed_nonce: ElementModQ,
    interpret_ballot: bool = True,
) -> Optional[CiphertextBallot]:
    """
    Given a ballot and the necessary election context, encrypts the ballot and returns the
    ciphertext. If anything goes wrong, `None` is returned. If the number of selections is greater
    than allowed for this ballot style, we call that an "overvoted" ballot. There's no valid
    encryption of that, so we normally "interpret" the ballot first, replacing the votes with an
    undervote (i.e., all selections blank). The `interpret_ballot` flag, if false, overrides this
    behavior, allowing for the creation of malformed ballot encryptions.
    """

    # TODO: implement this for part 2, be sure to use your encrypt_selection from part 1.
    raise RuntimeError("not implemented yet")


def encrypt_ballots(
    context: AnyElectionContext, ballots: List[PlaintextBallot], seed_nonce: ElementModQ
) -> Optional[List[CiphertextBallot]]:
    """
    Given a list of ballots and the necessary election context, encrypts the ballots and returns
    a list of the ciphertexts. If anything goes wrong, `None` is returned. This also ensures that
    the nonce seeds are unique for each ballot.
    """

    # TODO: implement this for part 2. Be sure to use your encrypt_ballot.
    raise RuntimeError("not implemented yet")


def validate_encrypted_selection(
    context: AnyElectionContext, selection: CiphertextSelection
) -> bool:
    """Validates the proof on an encrypted selection. Returns true if everything is good."""

    # TODO: implement this for part 1.
    raise RuntimeError("not implemented yet")


def validate_encrypted_ballot(
    context: AnyElectionContext, ballot: CiphertextBallot
) -> bool:
    """Validates all the proofs on the encrypted ballot. Returns true if everything is good."""

    # TODO: implement this for part 2. Be sure to use your validate_encrypted_selection from part 1.
    raise RuntimeError("not implemented yet")


def decrypt_selection(
    context: PrivateElectionContext,
    selection: CiphertextSelection,
    seed: ElementModQ = rand_range_q(1),
) -> Optional[PlaintextSelectionWithProof]:
    """
    Given an encrypted selection and the necessary crypto context, decrypts it, returning
    the plaintext selection along with a Chaum-Pedersen proof of its correspondence to the
    ciphertext. The optional seed is used for computing the proof.
    """

    # TODO: implement this for part 1
    raise RuntimeError("not implemented yet")


def decrypt_ballot(
    context: PrivateElectionContext,
    ballot: CiphertextBallot,
    seed: ElementModQ = rand_range_q(1),
) -> Optional[PlaintextBallotWithProofs]:
    """
    Given an encrypted ballot and the necessary crypto context, decrypts it. Each
    decryption includes the necessary Chaum-Pedersen decryption proofs as well.
    The optional seed is used for the decryption proofs.
    """

    # TODO: implement this for part 2. Be sure to use your decrypt_selection from part 1.
    raise RuntimeError("not implemented yet")


def validate_decrypted_selection(
    context: AnyElectionContext,
    plaintext: PlaintextSelectionWithProof,
    ciphertext: CiphertextSelection,
) -> bool:
    """
    Validates that the plaintext is provably generated from the ciphertext. Returns true
    if everything is good.
    """

    # TODO: implement this for part 1.
    raise RuntimeError("not implemented yet")


def validate_decrypted_ballot(
    context: AnyElectionContext,
    plaintext: PlaintextBallotWithProofs,
    ciphertext: CiphertextBallot,
) -> bool:
    """Validates that the plaintext is provably generated from the ciphertext. Returns true if everything is good."""

    # TODO: implement this for part 2. Be sure to use your validate_decrypted_selection from part 1.
    raise RuntimeError("not implemented yet")


def tally_encrypted_ballots(
    context: AnyElectionContext, ballots: List[CiphertextBallot]
) -> List[CiphertextSelectionTally]:
    """Homomorphically accumulates the encrypted ballots, returning list of tallies, one per selection."""

    # TODO: implement this for part 2.
    raise RuntimeError("not implemented yet")


def decrypt_tally(
    context: PrivateElectionContext,
    selection: CiphertextSelectionTally,
    seed: ElementModQ = rand_range_q(1),
) -> Optional[PlaintextSelectionWithProof]:
    """Given an encrypted, tallied selection, and the necessary crypto context, decrypts it,
    returning the plaintext selection along with a Chaum-Pedersen proof of its correspondence to the
    ciphertext. The optional seed is used for computing the proof."""

    # TODO: implement this for part 2.
    raise RuntimeError("not implemented yet")


def decrypt_tallies(
    context: PrivateElectionContext,
    tally: List[CiphertextSelectionTally],
    seed: ElementModQ = rand_range_q(1),
) -> Optional[List[PlaintextSelectionWithProof]]:
    """Given a list of encrypted tallies and the necessary crypto context, does the
    decryption on the entire list. The optional seed is used for computing the proofs."""

    # TODO: implement this for part 2. Be sure to use decrypt_tally.
    raise RuntimeError("not implemented yet")


def validate_tally(
    context: AnyElectionContext,
    tally_plaintext: PlaintextSelectionWithProof,
    tally_ciphertext: CiphertextSelectionTally,
) -> bool:
    """Validates that the plaintext is provably generated from the ciphertext. Returns true if everything is good."""

    # TODO: implement this for part 2. It's similar to, but not the same as validate_decrypted_ballot.
    raise RuntimeError("not implemented yet")


def validate_tallies(
    context: AnyElectionContext,
    tally_plaintext: List[PlaintextSelectionWithProof],
    tally_ciphertext: List[CiphertextSelectionTally],
) -> bool:
    """Validates that the plaintext is provably generated from the ciphertext for every tally. Returns true if
    everything is good."""

    # TODO: implement this for part 2. Be sure to use validate_tally.
    raise RuntimeError("not implemented yet")


def tally_plaintext_ballots(
    context: AnyElectionContext, ballots: List[PlaintextBallot]
) -> PlaintextBallot:
    """Given a list of ballots, adds their counters and returns a ballot representing the totals of the contest."""

    # You may find this method to be handy. We use it for some unit tests.

    totals: Dict[str, int] = {}
    for b in ballots:
        for s in b.selections:
            if s.name not in totals:
                totals[s.name] = s.choice
            else:
                totals[s.name] += s.choice

    return PlaintextBallot(
        "TOTALS", [PlaintextSelection(name, totals[name]) for name in context.names]
    )
